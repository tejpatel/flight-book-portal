import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  ReactiveFormsModule
} from "@angular/forms";
import { Router } from "@angular/router";
import { AutheticationService } from "../service/authetication.service";
import { ToastrService } from "ngx-toastr";
import { NgxSpinner } from "ngx-spinner/lib/ngx-spinner.enum";
import { timeout } from "rxjs/operators";
import { NgxSpinnerService } from "ngx-spinner";
import { Login } from "../models/login.model";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginform: FormGroup;
  public submitted: boolean = false;
  user: [];
  constructor(
    private router: Router,
    private _authService: AutheticationService,
    private toast: ToastrService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.loginform = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    });
  }
  public temp: Login[];

  onLoginUser(loginform) {
    this.user = loginform.value;
    // console.log(this.user);
    this.submitted = true;
    if (this.loginform.invalid) {
      this.toast.error("Please Enter Valid Id and Password");
      return;
    } else {
      this.spinner.show();
      this._authService.logged = true;
      this._authService
        .Validate(loginform.value.email, loginform.value.password)
        .subscribe(
          res => {
            if (res.success) {
              this.toast.success("Login Successfully");

              let temp = res.data;
              //  console.log(temp.map(result => result.token));
              // Login temp = res.data;
              console.log(temp);
              // alert(temp);
              this._authService.storeToken(temp.token);
              setTimeout(() => this.spinner.hide(), 1000);

              this.router.navigate(["/home"]);
            }
          },
          error => {
            //alert("PLease Enter Valid Id and Password");
            setTimeout(() => this.spinner.hide(), 1000);

            this.toast.warning(error);
          },
          () => {}
        );

      // }

      // .login(this.loginform.value.email,this.loginform.value.password)
      // {

      // }
    }
  }
  onClose() {
    this._authService.logged = false;
    this.router.navigate(["/"]);
  }
}
