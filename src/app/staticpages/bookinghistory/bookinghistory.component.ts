import { Component, OnInit } from "@angular/core";
import { BookingService } from "src/app/service/booking.service";
import { element } from "protractor";
import { AutheticationService } from "src/app/service/authetication.service";
import { Route } from "@angular/compiler/src/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { GetBookingData } from "src/app/models/getbookingdata.model";
import { ResponseFormatModel } from "src/app/models/responseFormat.model";

@Component({
  selector: "app-bookinghistory",
  templateUrl: "./bookinghistory.component.html",
  styleUrls: ["./bookinghistory.component.css"]
})
// export class BookingHistoryInterface {
//   _id: any;
//   tripType: any;
//   fromPlace: any;
//   toPlace: any;
//   travelDate: any;
//   returnDate: any;
//   adultsCount: any;
//   childrenCount: any;
//   infantsCount: any;
//   travelClass: any;
//   userId: any;
//   status: any;
// }
export class BookinghistoryComponent implements OnInit {
  BookingData: GetBookingData[];
  status: any;
  isCancel: boolean;
  totalRec: number;
  page: number = 1;
  searchText;
  // BookingData : GetBookingData[];

  constructor(
    private _bookingservice: BookingService,
    private _auth: AutheticationService,
    private route: Router,
    private toast: ToastrService
  ) {}

  ngOnInit() {
    if (this._auth.getToken() === null) {
      alert("Please Log In First");
      this.route.navigate(["/login"]);
    } else {
      this._bookingservice.getBookingData().subscribe(
        res => {
          // res.map(data => res);
          if (res.success) {
            this.BookingData = res.data;
            this.totalRec = this.BookingData.length;
            console.log(this.BookingData);
            this.status = res.data.map(element =>
              element.status === "cancelled"
                ? (this.isCancel = false)
                : (this.isCancel = true)
            );
            console.log(this.status);
            console.log(this.BookingData);
            // console.log(res.B.status);

            for (let ele of this.status) {
              if (ele === "cancelled") {
                this.isCancel = false;
              }
              if (ele === "booked") {
                this.isCancel = true;
              }

              console.log(ele);
            }
          } else {
            // console.log(JSON.stringify(res));
          }
          // console.log(res);
          // // this.BookingData
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  onCancelled(bookingid) {
    console.log(bookingid);
    this._bookingservice.cancelBooking(bookingid).subscribe(
      res => {
        // console.log(this.BookingData.filter(ele => ele.id));

        if (res) {
          // alert(JSON.stringify(res.data));
          this._bookingservice.getBookingData().subscribe(res => {
            if (res.success) {
              this.toast.warning("cancelled Successfully");
              this.BookingData = res.data;
            }
          });

          // console.log(this.getDataById(bookingid));
        }
      },
      error => console.log(error)
    );
  }
}
