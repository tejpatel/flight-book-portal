export const data = [
    {
        "id": "1",
        "name": "Tej Patel",
        "imageUrl": "https://ui-avatars.com/api/?rounded=true"
    },
    {
        "id": "2",
        "name": "Deep Trivedi",
        "imageUrl": "https://ui-avatars.com/api/?rounded=true"
    },
    {
        "id": "3",
        "name": "Mayank Vamja",
        "imageUrl": "https://ui-avatars.com/api/?rounded=true"
    },
    {
        "id": "4",
        "name": "Abhay Manvar",
        "imageUrl": "https://ui-avatars.com/api/?rounded=true"
    },
    {
        "id": "5",
        "name": "Akash Ladani",
        "imageUrl": "https://ui-avatars.com/api/?rounded=true"
    },
    {
        "id": "6",
        "name": "Ronak Baldha",
        "imageUrl": "https://ui-avatars.com/api/?rounded=true"
    },
    {
        "id": "7",
        "name": "Abhishek Vyas",
        "imageUrl": "https://ui-avatars.com/api/?rounded=true"
    },
    {
        "id": "8",
        "name": "Ramyank Sanghavi",
        "imageUrl": "https://ui-avatars.com/api/?rounded=true"
    },
    {
        "id": "9",
        "name": "AkhbarHusen Patel",
        "imageUrl": "https://ui-avatars.com/api/?rounded=true"
    },
    {
        "id": "10",
        "name": "Naimesh Vaja",
        "imageUrl": "https://ui-avatars.com/api/?rounded=true"
    }
]