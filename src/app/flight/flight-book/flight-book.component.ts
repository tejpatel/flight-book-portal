import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2
} from "@angular/core";
import {
  ReactiveFormsModule,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { AutheticationService } from "src/app/service/authetication.service";
import { BookingService } from "src/app/service/booking.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-flight-book",
  templateUrl: "./flight-book.component.html",
  styleUrls: ["./flight-book.component.css"]
})
export class FlightBookComponent implements OnInit {
  constructor(
    private _authservice: AutheticationService,
    private _bookingservice: BookingService,
    private route: Router,
    private rennder: Renderer2
  ) {}

  @ViewChild("mybox", { static: false }) mybox: ElementRef;

  flightbookform: FormGroup;
  flightbook: any = [];

  isformsubmitted = false;
  // isvalid=false;

  ngOnInit() {
    window.onload = () => {
      // document.getElementById("myDate").min=new Date().getFullYear+'-'+(new Date().getMonth()+1)+'-'+new Date().getDate();
      let d = new Date().getDate();
      let dayString: string;
      d < 10 ? (dayString = "0" + d) : (dayString = d + "");

      let monthString: string;
      var m = new Date().getMonth() + 1;
      m < 10 ? (monthString = "0" + m) : (monthString = m + "");
      var temp = new Date().getFullYear() + "-" + monthString + "-" + dayString;
      console.log(temp);

      document.getElementById("myDate1").setAttribute("min", temp);
      document.getElementById("myDate2").setAttribute("min", temp);
      // document.getElementById("myDate").min="2020-08-21";
    };

    this.flightbookform = new FormGroup({
      tripType: new FormControl("round-trip", Validators.required),
      fromPlace: new FormControl(null, Validators.required),
      toPlace: new FormControl(null, Validators.required),
      travelDate: new FormControl(null, Validators.required),
      returnDate: new FormControl(Validators.required),
      adultsCount: new FormControl(null, Validators.required),
      childrenCount: new FormControl(null, Validators.required),
      infantsCount: new FormControl(null, Validators.required),
      travelClass: new FormControl(null, Validators.required)
    });
  }

  onBookFlight(flightbookform) {
    this.isformsubmitted = true;
    if (flightbookform.valid) {
      if (this._authservice.isAuthenticated() == true) {
        console.log("you are authticated");
      } else {
        alert("You Need login First");
        this.route.navigate(["/"]);
      }
      console.log(flightbookform.value);
      if (this._authservice.getToken() != null)
        this.flightbook.push({
          tripType: flightbookform.value.tripType,
          fromPlace: flightbookform.value.fromPlace,
          toPlace: flightbookform.value.toPlace,
          travelDate: flightbookform.value.travelDate,
          returnDate: flightbookform.value.returnDate,
          adultsCount: flightbookform.value.adultsCount,
          childrenCount: flightbookform.value.childrenCount,
          infantsCount: flightbookform.value.infantsCount,
          travelClass: flightbookform.value.travelClass
        });
      console.log(this.flightbook);
    } else {
      alert("Please Fill Up Form First");
    }

    this._bookingservice
      .bookFlight(
        flightbookform.value.tripType,
        flightbookform.value.fromPlace,
        flightbookform.value.toPlace,
        flightbookform.value.travelDate,
        flightbookform.value.returnDate,
        flightbookform.value.adultsCount,
        flightbookform.value.childrenCount,
        flightbookform.value.infantsCount,
        flightbookform.value.travelClass
      )
      .subscribe(res => {
        // console.log(this.flightbook);
        console.log(res);
      });
  }

  ngAfterViewInit(): void {
    //  this.rennder.setStyle(this.mybox.nativeElement, "backgroundColor", "red");
    const text = this.rennder.createText("Flight Booking Details");
  }
}
