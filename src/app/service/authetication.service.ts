import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Login } from "../models/login.model";
import { ResponseFormatModel } from "../models/responseFormat.model";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AutheticationService {
  constructor(private http: HttpClient) {}
  // BASE_URL="http://10.12.40.85:3000";
  logged: boolean;
  createUser(
    email1: any,
    password1: any,
    firstname1: any,
    lastname1: any,
    mobile_number1: any,
    gender1: any
  ) {
    let httpHeaders = new HttpHeaders().set("Content-Type", "application/json");
    let options = {
      headers: httpHeaders
    };
    const user = {
      email: email1,
      password: password1,
      firstName: firstname1,
      lastName: lastname1,
      mobile: mobile_number1,
      gender: gender1
    };
    // const user =JSON.stringify( {email:email1,password:password1,firstName:firstname1,lastName:lastame1,mobile:mobile_number1,gender:gender1});
    console.log(user);
    return this.http.post(
      "http://10.12.40.85:3000/users/register",
      {
        email: email1,
        password: password1,
        firstName: firstname1,
        lastName: lastname1,
        mobile: mobile_number1,
        gender: gender1
      },
      options
    );
  }

  Validate(username, password): Observable<ResponseFormatModel<Login>> {
    let httpHeaders = new HttpHeaders().set("Content-Type", "application/json");
    let options = {
      headers: httpHeaders
    };

    return this.http.post<ResponseFormatModel<Login>>(
      "http://10.12.40.85:3000/users/login",
      { email: username, password: password },
      options
    );
  }

  public isAuthenticated() {
    return this.getToken() != null;
  }
  getToken() {
    return localStorage.getItem("token");
  }
  storeToken(token) {
    return localStorage.setItem("token", token);
  }
  removeToken() {
    return localStorage.removeItem("token");
  }
  // isAutheticated() {
  //   if (localStorage.getItem("token") !== null) return true;
  //   else return false;
  // }
}

// login(username:string,pasword:string)
// {
//   return this.http.post('http://10.12.40.85:3000/users/login')
// }
